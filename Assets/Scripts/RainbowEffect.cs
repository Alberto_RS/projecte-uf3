using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainbowEffect : MonoBehaviour
{
    public float rainbowSpeed;

    private float hue;
    private float saturation;
    private float brightness;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        Color.RGBToHSV(spriteRenderer.material.color, out hue, out saturation, out brightness);
        hue += (Time.deltaTime * 0.25f) / rainbowSpeed;
        if(hue >= 1)
        {
            hue = 0;
        }
        saturation = 1;
        brightness = 1;
        spriteRenderer.material.color = Color.HSVToRGB(hue, saturation, brightness);
    }
}
