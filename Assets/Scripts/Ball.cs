using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed = 8;
    public Rigidbody2D rigidbody2D;
    public Vector2 startPos;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = startPos;
        Launch();
    }

    public void Reset()
    {
        transform.position = startPos;
        rigidbody2D.velocity = Vector2.zero;
        speed = 8;
        Launch();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            speed = speed + 2;
        }
    }

    public void Launch()
    {
        float x = Random.Range(0, 2) == 0 ? -1 : 1;
        float y = Random.Range(0, 2) == 0 ? -1 : 1;

        rigidbody2D.velocity = new Vector2(speed * x, speed * y);
    }
}
