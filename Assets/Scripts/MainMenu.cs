using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void PvE()
    {
        SceneManager.LoadScene("PvE");
    }

    public void PvP()
    {
        SceneManager.LoadScene("PvP");
    }


}
