using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Players : MonoBehaviour
{
    public bool player1;
    public float speed = 5;
    public Rigidbody2D Rigidbody2D;

    private float move;
    private Vector2 startPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(player1)
        {
            move = Input.GetAxisRaw("Vertical");
        }
        else
        {
            move = Input.GetAxisRaw("Vertical2");
        }
        
        Rigidbody2D.velocity = new Vector2(Rigidbody2D.velocity.x, move * speed);
    }

    public void Reset()
    {
        Rigidbody2D.velocity = Vector2.zero;
        transform.position = startPos;  
    }
}
